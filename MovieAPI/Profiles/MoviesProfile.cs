﻿using AutoMapper;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTO.Movie;
using System.Linq;

namespace MovieAPI.Profiles
{
    public class MoviesProfile : Profile
    {
        public MoviesProfile()
        {
        CreateMap<Movie, MoviesReadDTO>()
                .ForMember(MoviesReadDTO => MoviesReadDTO.Character, options => options
                .MapFrom(f => f.Characters.Select(f => $"/api/v1/Characters/{f.Id}").ToList()))
                .ReverseMap();
            CreateMap<Movie, MovieCreateDTO>()
                .ReverseMap();
            CreateMap<Movie, MovieEditDTO>()
                .ReverseMap();
        }
    }
}
