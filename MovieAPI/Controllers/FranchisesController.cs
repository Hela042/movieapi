﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTO.Franchise;
using MovieAPI.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseService _franchiseService;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseService franchiseService, IMapper mapper)
        {
            _franchiseService = franchiseService;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetches all franchises with ids of movies and characters in those franchises.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetFranchisesAsync());
        }

        /// <summary>
        /// Fetches a specific franchise by id.
        /// </summary>
        /// <param name="id">Franchise id.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchiseById(int id)
        {


            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            var domainFranchise = await _franchiseService.GetFranchiseByIdAsync(id);
            return _mapper.Map<FranchiseReadDTO>(domainFranchise);
        }

        /// <summary>
        /// Adds the sent franchise into the database.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ActionResult<FranchiseReadDTO>> PostFranchise([FromBody] FranchiseCreateDTO franchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);
            try
            {
                domainFranchise=  await _franchiseService.AddFranchiseAsync(domainFranchise);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            FranchiseReadDTO newFranchise = _mapper.Map<FranchiseReadDTO>(domainFranchise);
            
            
            return CreatedAtAction("GetFranchiseById", new { id = newFranchise.Id }, newFranchise);
        }

        /// <summary>
        /// Updates the specified franchise with the sent data.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status418ImATeapot)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutFranchise(int id, [FromBody] FranchiseEditDTO franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }
            Franchise domainFranchise = _mapper.Map<Franchise>(franchise);

            try
            {
                await _franchiseService.UpdateFranchiseAsync(domainFranchise);
            }
            catch (DbUpdateConcurrencyException)
            {
                if (_franchiseService.FranchiseExists(id))
                {
                    return StatusCode(StatusCodes.Status418ImATeapot);
                }
                else
                {
                    return NotFound();
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Deletes the specified franchise from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            try
            {
                await _franchiseService.DeleteFranchiseAsync(id);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }
            catch (DbUpdateException)
            {
                return BadRequest("Franchise has movies, remove all movies from the franchise.");
            }
            
            return NoContent();
        }


        /// <summary>
        /// Updates the movies contained in a franchise with the movie ids sent in the body.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpPut("{id}/movies")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateFranchiseMovies(int id, [FromBody] int[] movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.UpdateFranchiseMoviesAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }
            return NoContent();
        }

        /// <summary>
        /// Removes the passed in movies from the specific franchise.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movies"></param>
        /// <returns></returns>
        [HttpDelete("{id}/movies")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> RemoveMovieFranchise(int id, [FromBody] int[] movies)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            try
            {
                await _franchiseService.RemoveMovieFranchiseAsync(id, movies);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest();
            }

            return NoContent();
        }
    }
}
