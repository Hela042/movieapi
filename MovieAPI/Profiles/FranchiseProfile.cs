﻿using AutoMapper;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTO.Franchise;
using System.Linq;

namespace MovieAPI.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(franchiseDTO => franchiseDTO.Movies, options => options
                .MapFrom(f => f.Movies.Select(f => $"/api/v1/Movies/{f.Id}").ToList()))
                .ForMember(franchiseDTO => franchiseDTO.Characters, options => options
                .MapFrom(f => f.Movies.SelectMany(m => m.Characters.Select(c => $"/api/v1/Characters/{c.Id}")).ToList()))
                .ReverseMap();
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ReverseMap();
            CreateMap<Franchise, FranchiseEditDTO>()
                .ReverseMap();
        }
    }
}