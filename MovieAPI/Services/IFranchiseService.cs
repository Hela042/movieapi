﻿using MovieAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public interface IFranchiseService
    {
        public Task<IEnumerable<Franchise>> GetFranchisesAsync();
        public Task<Franchise> GetFranchiseByIdAsync(int id);
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseAsync(Franchise franchise);
        public Task UpdateFranchiseMoviesAsync(int id, int[] movies);
        public Task DeleteFranchiseAsync(int id);
        public Task RemoveMovieFranchiseAsync(int id, int [] movies);
        public bool FranchiseExists(int id);
    }
}
