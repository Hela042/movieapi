﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MovieAPI.Models.Domain
{
    public class Movie
    {
        //PK
        public int Id { get; set; }
        //Fields
        [Required]
        [MaxLength(50)] //How long it can be
        public string Title { get; set; }
        [Required]
        [MaxLength(150)]
        public string Genre { get; set; }
        [Required]
        [MaxLength(5)]
        [Column(TypeName = "varchar(5)")]
        public string ReleaseYear { get; set; }
        [Required]
        [MaxLength(70)]
        public string Director { get; set; }
        [MaxLength(250)]
        public string Picture { get; set; }
        [MaxLength(250)]
        public string Trailer { get; set; }
        //Relationships
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
        public ICollection<Character> Characters { get; set; }
        
    }
}
