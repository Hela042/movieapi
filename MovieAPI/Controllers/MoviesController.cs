﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTO.Movie;
using MovieAPI.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        private readonly IMapper _mapper;
        public MoviesController(IMovieService movieService, IMapper mapper)
        {
            _movieService = movieService;
            _mapper = mapper;
        }
        /// <summary>
        /// Update what Characters are in the Movies
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPost("{id}/Movie")]
        [ProducesResponseType(StatusCodes.Status204NoContent)] //What status codes the request can give
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> UpdateCharactersInMovie(int id, List<int> movie)
        {

            try
            {
                await _movieService.UpdateCharacterMoviesAsync(id, movie);
            }
            catch (KeyNotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }

        /// <summary>
        /// Get all Movies
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<MoviesReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MoviesReadDTO>>(await _movieService.GetMoviesAsync());
        }

        /// <summary>
        /// Get a selected Movie using the id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<MoviesReadDTO>> GetMovieById(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            return _mapper.Map<MoviesReadDTO>(await _movieService.GetMovieByIdAsync(id));
        }

        /// <summary>
        /// Post a new Movie
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<MoviesReadDTO>>PostMovie(MovieCreateDTO movie)
        {
            Movie domainMovie = _mapper.Map<Movie>(movie);
            try
            {
                domainMovie = await _movieService.AddMovieAsync(domainMovie);
            }
            catch (System.Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            
            MoviesReadDTO newMovie = _mapper.Map <MoviesReadDTO>(domainMovie);
            
            return CreatedAtAction("GetMovieById", new { id = newMovie.Id }, newMovie);
        }

        /// <summary>
        /// Update an existing Movie using the id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status418ImATeapot)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }
            Movie domainMovie = _mapper.Map<Movie>(movie);

            try
            {
                await _movieService.UpdateMovieAsync(domainMovie);
            }
            catch (DbUpdateConcurrencyException)
            {

                if (_movieService.MovieExists(id))
                {
                    return StatusCode(StatusCodes.Status418ImATeapot);
                }
                else
                {
                    return NotFound();
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Delete a Movie
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            await _movieService.DeleteMovieAsync(id);
            return NoContent();
        }

    }
}