﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public class FranchiseService : IFranchiseService
    {
        private readonly MovieDbContext _context;

        public FranchiseService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            if (franchise == null)
            {
                throw new KeyNotFoundException();
            }
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.Id == id);
        }

        public async Task<Franchise> GetFranchiseByIdAsync(int id)
        {
            return await _context.Franchises.Include(f => f.Movies)
                    .ThenInclude(m => m.Characters)
                    .FirstOrDefaultAsync(f => f.Id == id);
        }

        public async Task<IEnumerable<Franchise>> GetFranchisesAsync()
        {
            return await _context.Franchises
                .Include(f => f.Movies)
                .ThenInclude(m => m.Characters)
                .ToListAsync();
        }

        public async Task RemoveMovieFranchiseAsync(int id, int[] movies)
        {
            Franchise franchiseToEdit = await _context.Franchises.Include(f => f.Movies).FirstOrDefaultAsync(f => f.Id == id);
            foreach (var movieId in movies)
            {
                Movie movieToUncouple = await _context.Movies.FindAsync(movieId);
                if (movieToUncouple == null)
                {
                    throw new KeyNotFoundException();
                }
                franchiseToEdit.Movies.Remove(movieToUncouple);
            }
            await _context.SaveChangesAsync();
        }

        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        
        public async Task UpdateFranchiseMoviesAsync(int id, int[] movies)
        {
            Franchise franchiseToUpdateMovies = await _context.Franchises
               .Include(f => f.Movies)
               .FirstOrDefaultAsync(f => f.Id == id);
            if (franchiseToUpdateMovies == null)
            {
                throw new KeyNotFoundException();
            }
            foreach (int movieId in movies)
            {
                Movie movie = await _context.Movies.FindAsync(movieId);
                if (movie == null)
                {
                    throw new KeyNotFoundException();
                }
                franchiseToUpdateMovies.Movies.Add(movie);
            }
            await _context.SaveChangesAsync();
        }
    }
}
