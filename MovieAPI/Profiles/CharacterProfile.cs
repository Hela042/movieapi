﻿using AutoMapper;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTO.Character;

namespace MovieAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ReverseMap();
            CreateMap<Character, CharacterCreateDTO>()
                .ReverseMap();
        }
    }
}
