# Assignment Create a Web API and document it

Assignment for Noroff Accelerate winter 2022. Task is to use entity framework Code first to create a database and then create a web API to interact with the database.

## Description

The first task is to create a databse with entity framework code first.
The second task is to create a web API using ASP .net, and create several endpoints to interact with the database.


## Getting Started

### Dependencies

* .Net Framework
* Visual Studio 2019/22 OR Visual Studio Code
* SQL Server

### Before Executing
* Remember to set your SQL server data source in the data source field in the connection string in the appsetting.json file

### Executing program
* clone repository / download
* Open solution in Visual Studio
* Build and run with IIS Express

## Author

Jessica Lindqvist [@Jessica.Lindqvist]
Lukas Mårtensson [@Hela042]
