﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using MovieAPI.Models.DTO.Character;
using MovieAPI.Services;
using System.Collections.Generic;
using System.Net.Mime;
using System.Threading.Tasks;

namespace MovieAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterService _characterService;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterService characterService, IMapper mapper)
        {
            _characterService = characterService;
            _mapper = mapper;
        }


        /// <summary>
        /// Fetches all characters.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetCharactersAsync());
        }

        /// <summary>
        /// Fetches the specified character by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacterById(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            return _mapper.Map<CharacterReadDTO>(await _characterService.GetCharacterByIdAsync(id));
        }

        /// <summary>
        /// Adds the posted character to the database.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<CharacterReadDTO>> PostCharacter([FromBody] CharacterCreateDTO character)
        {
            Character domainCharacter = _mapper.Map<Character>(character);

            try
            {
                domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);
            }
            catch
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            CharacterReadDTO newCharacter = _mapper.Map<CharacterReadDTO>(domainCharacter);

            return CreatedAtAction("GetCharacterById", new { id = newCharacter.Id }, newCharacter);
        }
        /// <summary>
        /// Updates the specified character by id.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status418ImATeapot)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PutCharacter(int id, [FromBody] CharacterEditDTO character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }
            Character domainCharacter = _mapper.Map<Character>(character);

            try
            {
                await _characterService.UpdateCharacterAsync(domainCharacter);
            }
            catch (DbUpdateConcurrencyException)
            {

                if (_characterService.CharacterExists(id))
                {
                    return StatusCode(StatusCodes.Status418ImATeapot);
                }
                else
                {
                    return NotFound();
                }
            }
            return NoContent();
        }

        /// <summary>
        /// Deletes the specified character by id from the database.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            await _characterService.DeleteCharacterAsync(id);
            return NoContent();
        }

    }
}
