﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models;
using MovieAPI.Models.Domain;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly MovieDbContext _context;

        public MovieService(MovieDbContext context)
        {
            _context = context;
        }

        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        public async Task<Movie> GetMovieByIdAsync(int id)
        {
            return await _context.Movies.Include(m => m.Characters).FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<IEnumerable<Movie>> GetMoviesAsync()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }

        public bool MovieExists(int id)
        {
            return _context.Movies.Any(c => c.Id == id);
        }

        public async Task UpdateCharacterMoviesAsync(int id, List<int> movies)
        {
            Character characterUpdateMovies = await _context.Characters
                .Include(c => c.Movies).Where(c => c.Id == id).FirstAsync();

            foreach (int characterId in movies)
            {
                Movie movieschar = await _context.Movies.FindAsync(characterId);
                if (movieschar == null)
                {
                    throw new KeyNotFoundException();
                }
                characterUpdateMovies.Movies.Add(movieschar);
            }

            await _context.SaveChangesAsync();
        }

        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
