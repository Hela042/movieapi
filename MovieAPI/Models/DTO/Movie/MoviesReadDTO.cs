using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models.DTO.Movie
{
    public class MoviesReadDTO //Everything that should display from the database
    {
        public int Id { get; set; }     
        public string Title { get; set; }     
        public string Genre { get; set; }       
        public string ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public List<string> Character { get; set; } //A list of the Characters
    }
}
