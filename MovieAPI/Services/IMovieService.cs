﻿using MovieAPI.Models.Domain;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MovieAPI.Services
{
    public interface IMovieService
    {
        public Task<IEnumerable<Movie>> GetMoviesAsync();
        public Task<Movie> GetMovieByIdAsync(int id);
        public Task<Movie> AddMovieAsync(Movie movie);
        public Task UpdateMovieAsync(Movie movie);
        public Task UpdateCharacterMoviesAsync(int id, List<int> movies);
        public Task DeleteMovieAsync(int id);
        public bool MovieExists(int id);
    }
}
