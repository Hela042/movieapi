﻿using Microsoft.EntityFrameworkCore;
using MovieAPI.Models.Domain;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace MovieAPI.Models
{
    public class MovieDbContext: DbContext
    {
        

        // Tables
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        public MovieDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // To do data seeding
            //Characters
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 1,
                FullName = "Tony Stark",
                Alias = "Iron Man",
                Gender = "Male",
                Picture = "https://marvelcinematicuniverse.fandom.com/wiki/Iron_Man?file=IronMan-EndgameProfile.jpg"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 2,
                FullName = "Dom Cobb",
                Gender = "Male",
                Picture = "https://static.wikia.nocookie.net/inception/images/2/2b/Dom_cobb.jpg/revision/latest/scale-to-width-down/606?cb=20150125061140"
            });
            modelBuilder.Entity<Character>().HasData(new Character
            {
                Id = 3,
                FullName = "Leia Organa",
                Alias = "Princess Leia",
                Gender = "Female",
                Picture = "https://en.wikipedia.org/wiki/Princess_Leia#/media/File:Princess_Leia's_characteristic_hairstyle.jpg"
            });
            //Movies
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 1,
                Title = "Iron Man",
                Genre = "Action, Adventure, Sci-Fi",
                ReleaseYear = "2008",
                Director = "Jon Favreau",
                Picture = "https://m.media-amazon.com/images/I/31UiPirP4GL._AC_SS450_.jpg",
                Trailer = "https://www.youtube.com/watch?v=8hYlB38asDY",
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 2,
                Title = "Iron Man 2",
                Genre = "Action, Adventure, Sci-Fi",
                ReleaseYear = "2010",
                Director = "Jon Favreau",
                Picture = "https://static.posters.cz/image/750/poster/iron-man-2-one-sheet-i7902.jpg",
                Trailer = "https://www.youtube.com/watch?v=BoohRoVA9WQ",
                FranchiseId = 1
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 3,
                Title = "Inception",
                Genre = "Action, Adventure, Sci-Fi, Thriller",
                ReleaseYear = "2010",
                Director = "Christopher Nolan",
                Picture = "https://m.media-amazon.com/images/M/MV5BMjAxMzY3NjcxNF5BMl5BanBnXkFtZTcwNTI5OTM0Mw@@._V1_.jpg",
                Trailer = "https://www.youtube.com/watch?v=YoHD9XEInc0"
            });
            modelBuilder.Entity<Movie>().HasData(new Movie
            {
                Id = 4,
                Title = "Star Wars: A New Hope",
                Genre = "Action, Adventure, Sci-Fi, Fantasy",
                ReleaseYear = "1977",
                Director = "George Lucas",
                Picture = "https://static.posters.cz/image/1300/poster/star-wars-classic-i97850.jpg",
                Trailer = "https://www.youtube.com/watch?v=vZ734NWnAHA",
                FranchiseId = 2
            });
            //Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 1,
                Name = "Marvel Cinematic Universe",
                Description = "Marvels saga of movies."
            });
            modelBuilder.Entity<Franchise>().HasData(new Franchise
            {
                Id = 2,
                Name = "Star Wars Franchise",
                Description = "Star Wars saga of movies."
            });
            // seed m2m relationships
            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharacterMovie",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MoviesId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharactersId"),
                    je =>
                    {
                        je.HasKey("CharactersId", "MoviesId");
                        je.HasData(
                            new { CharactersId = 1, MoviesId = 1 },
                            new { CharactersId = 1, MoviesId = 2 },
                            new { CharactersId = 2, MoviesId = 3 },
                            new { CharactersId = 3, MoviesId = 4 }
                            );
                    }
                 );
        }
    }
}
