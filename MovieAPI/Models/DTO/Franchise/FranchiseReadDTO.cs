﻿using System.Collections.Generic;

namespace MovieAPI.Models.DTO.Franchise
{
    public class FranchiseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Movies { get; set; }
        public List<string> Characters { get; set; }
    }
}
