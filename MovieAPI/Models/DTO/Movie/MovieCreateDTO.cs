﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieAPI.Models.DTO.Movie
{
    public class MovieCreateDTO //What needs to be filled to create a new movie
    {
        public string Title { get; set; }
        public string Genre { get; set; }
        public string ReleaseYear { get; set; } //Can not be longer then 5 chars
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
    }
}
