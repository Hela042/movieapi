﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models.Domain
{
    public class Character
    {
        //PK
        public int Id { get; set; }
        //Fields
        [Required]
        [MaxLength(100)]
        public string FullName { get; set; }
        [MaxLength(100)]
        public string Alias { get; set; }
        [Required]
        [MaxLength(20)]
        public string Gender { get; set; }
        [MaxLength(250)]
        public string Picture { get; set; }
        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}