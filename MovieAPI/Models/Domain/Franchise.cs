﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MovieAPI.Models.Domain
{
    public class Franchise
    {
        //PK
        public int Id { get; set; }
        //Fields
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(200)]
        public string Description { get; set; }
        //Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
